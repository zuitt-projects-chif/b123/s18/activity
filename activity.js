const trainer = {
  name: "Goh",
  age: 10,
  address: {
    city: "Vermilion City",
    country: "Japan",
  },
  friends: [],
  pokemons: [],
  catch: function (caughtPokemon) {
    if (this.pokemons.length <= 6) {
      this.pokemons.push(caughtPokemon);
      console.log(`Gotcha ${caughtPokemon.name}`);
    } else {
      // error message
      console.warn("A trainer should only have 6 pokemons to carry");
    }
  },
  release: function () {
    //remove last
    if (this.pokemons.length < 1) {
      console.warn("You have no more pokemons! Catch one first");
    } else {
      const removed = this.pokemons.pop();
      console.log(`Removed ${removed.name}`);
    }
  },
};

function Pokemon(name, type, level) {
  this.name = name;
  this.type = type;
  this.level = level;
  this.hp = level * 3;
  this.atk = level * 2.5;
  this.def = level * 2;
  this.isFainted = false;
  this.tackle = function (pokemonToTackle) {
    if (pokemonToTackle.isFainted !== true && pokemonToTackle.hp > 0) {
      console.info(`${this.name} tackled ${pokemonToTackle.name}`);
      const newHP = pokemonToTackle.hp - this.atk;
      console.warn(
        `${pokemonToTackle.name} hp reduced from ${pokemonToTackle.hp} to ${newHP}`
      );
      if (newHP < 0) {
        pokemonToTackle.faint();
      }
      pokemonToTackle.hp = newHP;
    } else {
      pokemonToTackle.faint();
    }
  };
  this.faint = function () {
    if (!this.isFainted) {
      console.error(`${this.name} has fainted`);
      this.isFainted = true;
    } else {
      console.error(
        `${this.name} has already fainted, please stop. Mo more T_T`
      );
    }
  };
}

const pokemon1 = new Pokemon("Togepi", "Psychic", 5);
const pokemon2 = new Pokemon("Pikachu", "Electric", 10);

console.log("Trainer", trainer);
console.log("pokemon1", pokemon1);
console.log("pokemon2", pokemon2);
